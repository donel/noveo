import { call, put, takeLatest, takeEvery } from 'redux-saga/effects'
import api from './api'
import { navigate } from './navigation_service'
import { feedSourceSchema } from './validation'
import * as ActionTypes from './actions'
import { SCREEN, MODAL } from './routes'
import { USER_NOTICE } from './strings'

function* fetchSourceFeed(action) {
  try {
    const { articleArr, articleCount, err } = yield call(
      api.fetchSourceFeed,
      action.payload.url
    )

    if (!err) {
      yield put({
        type: ActionTypes.FEED_SOURCE_FETCH_SUCCEEDED,
        payload: {
          articleArr,
          articleCount,
        },
      })
      yield call(navigate, { routeName: SCREEN.HOME })
    } else {
      yield put({
        type: ActionTypes.FEED_SOURCE_FETCH_FAILED,
        payload: { err },
      })
    }
  } catch (err) {
    yield put({
      type: ActionTypes.FEED_SOURCE_FETCH_FAILED,
      payload: {
        err: { location: err.message },
      },
    })
  }
}

function* onSourceFetchFail() {
  yield call(navigate, {
    routeName: MODAL.FEED_SOURCE_FAIL,
    params: { message: USER_NOTICE.REQUEST_FAILED },
  })
}

function* onSourceSubmit(action) {
  const { feedSource } = action.payload
  const valid = yield feedSourceSchema.isValid(feedSource)

  if (valid) {
    yield put({
      type: ActionTypes.SETUP_SOURCE_FEED_URL,
      payload: { url: feedSource },
    })
  } else {
    const erorrs = yield feedSourceSchema
      .validate(feedSource)
      .catch(err => err.message)

    yield call(navigate, {
      routeName: MODAL.FEED_SOURCE_FAIL,
      params: { message: erorrs },
    })
  }
}

function* sagas() {
  yield takeLatest(ActionTypes.FETCH_FEED_SOURCE_REQUESTED, fetchSourceFeed)
  yield takeEvery(ActionTypes.FEED_SOURCE_FETCH_FAILED, onSourceFetchFail)
  yield takeEvery(ActionTypes.ON_SOURCE_SUBMIT, onSourceSubmit)
}

export default sagas
