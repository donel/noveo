import { NavigationActions } from 'react-navigation'
import { SCREEN } from './routes'

let _navigator

export function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef
}

export function navigate({ routeName = SCREEN.HOME, params = {} }) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  )
}
