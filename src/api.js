import axios from 'axios'
import _ from 'lodash'
import moment from 'moment'
import { ERR_LOCATION } from './strings'

async function fetchSourceFeed(url) {
  try {
    const response = await axios(url)
    const statusCode = _.get(response, ['status'])
    const articleArr = _.get(response, ['data', 'feed', 'article'])
    const articleCount = _.size(articleArr)

    if (statusCode === 200 && articleCount) {
      const articleStr = JSON.stringify(articleArr).replace(/<[^>]*>?/gm, '')
      const articleArrSortedByTime = _.sortBy(
        JSON.parse(articleStr),
        article => -moment(article.date).unix()
      )

      return {
        statusCode,
        articleArr: articleArrSortedByTime,
        articleCount,
        err: null,
      }
    }
    return {
      err: {
        location: ERR_LOCATION.FETCH_SOURCE_FEED,
        statusCode,
        articleCount,
      },
    }
  } catch (error) {
    throw new Error(ERR_LOCATION.FETCH_SOURCE_FEED, error)
  }
}

const api = {
  fetchSourceFeed,
}

export default api
