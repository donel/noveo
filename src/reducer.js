import * as ActionTypes from './actions'

const initialState = {
  url:
    'https://gist.githubusercontent.com/happy-thorny/bd038afd981be300ac2ed6e5a8ad9f3c/raw/dd90f04475a2a7c1110151aacc498eabe683dfe4/memes.json',
  err: '',
  articleArr: [],
  articleCount: 0,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.FEED_SOURCE_FETCH_SUCCEEDED:
      const { articleArr, articleCount } = action.payload
      return {
        ...state,
        articleArr,
        articleCount,
      }
    case ActionTypes.FEED_SOURCE_FETCH_FAILED:
      const { err } = action.payload
      return {
        ...state,
        err,
      }
    case ActionTypes.SETUP_SOURCE_FEED_URL:
      const { url } = action.payload
      return {
        ...state,
        url,
      }
    default:
      return state
  }
}

export default reducer
