import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { validateUrl } from '../validation'

const ArticleImage = ({ url }) => {
  const [isUrlValid, setUrlValid] = useState(false)
  const [isBgWhite, setBgWhite] = useState(true)

  useEffect(() => {
    if (validateUrl(url)) setUrlValid(true)
    setBgWhite(true)
  }, [url])

  return (
    <ImgContainer>
      {isUrlValid && (
        <Img
          resizeMode="cover"
          source={{ uri: url }}
          onLoad={() => setBgWhite(false)}
          isBgWhite={isBgWhite}
        />
      )}
    </ImgContainer>
  )
}

export default ArticleImage

const Img = styled.Image`
  width: 100%;
  height: 100%;
  background-color: ${props => (props.isBgWhite ? '#fff' : '#66dafb')};
`
const ImgContainer = styled.View`
  width: 60;
  height: 60;
`
