import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import reducer from '../reducer'
import sagas from '../sagas'
import Home from '../screens/Home'
import Details from '../screens/Details'
import FeedSource from '../screens/FeedSource'
import FeedSourceFail from '../modals/FeedSourceFail'
import { setTopLevelNavigator } from '../navigation_service'
import Reactotron from '../../ReactotronConfig'
import { SCREEN } from '../routes'

const sagaMonitor = Reactotron.createSagaMonitor()
const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
const middleware = applyMiddleware(sagaMiddleware)
const store = createStore(
  reducer,
  compose(
    middleware,
    Reactotron.createEnhancer()
  )
)

sagaMiddleware.run(sagas)

const MainStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    Details: {
      screen: Details,
    },
    FeedSource: {
      screen: FeedSource,
    },
  },
  { initialRouteName: SCREEN.HOME }
)

const RootStack = createStackNavigator(
  {
    Main: {
      screen: MainStack,
    },
    FeedSourceFail: {
      screen: FeedSourceFail,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
)

const AppContainer = createAppContainer(RootStack)

const App = () => (
  <Provider store={store}>
    <AppContainer
      ref={navigatorRef => {
        setTopLevelNavigator(navigatorRef)
      }}
    />
  </Provider>
)

export default App
