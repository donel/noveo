import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import styled from 'styled-components'
import ArticleImage from './ArticleImage'
import { SCREEN } from '../routes'
import Title from '../components/Title'

const ArticleCard = ({ navigation, article }) => {
  const { imageUrl = '', title = '', shortDescription = '' } = article

  return (
    <TouchableContainer
      style={styles.shadowBox}
      onPress={() => navigation.navigate(SCREEN.DETAILS, { article })}
    >
      <Card>
        <ArticleImage url={imageUrl} />

        <TextContent>
          <Title title={title} />

          <ShortDescription>{shortDescription}</ShortDescription>
        </TextContent>
      </Card>
    </TouchableContainer>
  )
}

export default ArticleCard

const Card = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 10px 0;
`
const TextContent = styled.View`
  display: flex;
  align-items: center;
  flex-basis: 55%;
  margin-left: 10px;
`
const ShortDescription = styled.Text`
  text-align: center;
`
const TouchableContainer = styled(TouchableOpacity)`
  background-color: #fff;
  margin: 10px 0;
  padding: 4px 10px;
`
const styles = StyleSheet.create({
  shadowBox: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 4,
  },
})
