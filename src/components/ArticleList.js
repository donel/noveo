import React from 'react'
import { Text } from 'react-native'
import styled from 'styled-components'
import ArticleCard from './ArticleCard'
import { USER_NOTICE } from '../strings'

const ArticleList = ({ articleCount, articleArr, navigation }) => {
  if (!articleCount) return <Text>{USER_NOTICE.SOURCE_NOT_DEFINED}</Text>

  return (
    <Container>
      {articleArr.map((article, index) => (
        <ArticleCard
          article={article}
          key={`article-${index}`}
          navigation={navigation}
        />
      ))}
    </Container>
  )
}

export default ArticleList

const Container = styled.View`
  display: flex;
  align-items: center;
  margin: 10px 0;
`
