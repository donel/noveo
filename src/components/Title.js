import React from 'react'
import styled from 'styled-components'

const Title = ({ title }) => <Container>{title}</Container>

export default Title

const Container = styled.Text`
  margin-bottom: 8px;
  font-weight: 600;
  margin: auto;
`
