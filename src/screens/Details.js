import React from 'react'
import { Linking, ScrollView, Button } from 'react-native'
import styled from 'styled-components'
import moment from 'moment'
import _ from 'lodash'
import { validateDate, validateUrl } from '../validation'
import ArticleImage from '../components/ArticleImage'
import { BUTTON_TITLE } from '../strings'
import Title from '../components/Title'

const PublicationDate = ({ date }) => {
  if (!validateDate(date)) return null
  return <Date>{moment(date).format('LLLL')}</Date>
}

const ArticleLink = ({ link }) => {
  if (!validateUrl(link)) return null
  return (
    <LinkContainer>
      <Button
        title={BUTTON_TITLE.GO_ARTICLE}
        onPress={() => Linking.openURL(link)}
      />
    </LinkContainer>
  )
}

const DetailsScreen = ({ navigation }) => {
  const article = _.get(navigation.state.params, 'article')
  const {
    description = '',
    title = '',
    imageUrl = '',
    date = '',
    link = '',
  } = article

  return (
    <ScrollView>
      <Container>
        <TitleContainer>
          <ImageContainer>
            <ArticleImage url={imageUrl} />
          </ImageContainer>

          <Title title={title} />
        </TitleContainer>

        <FullDescription>{description}</FullDescription>

        <PublicationDate date={date} />
        <ArticleLink link={link} />
      </Container>
    </ScrollView>
  )
}

export default DetailsScreen

const Container = styled.View`
  display: flex;
  margin: 20px 30px;
`
const TitleContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: 5px;
`
const ImageContainer = styled.View`
  flex: 0;
`
const FullDescription = styled.Text`
  margin: 20px 0px;
`
const Date = styled.Text`
  margin: 10px 0px;
  font-size: 10px;
`
const LinkContainer = styled.View`
  margin-top: 10px;
`
