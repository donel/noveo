import React, { useState, useEffect } from 'react'
import { TextInput, Button } from 'react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import _ from 'lodash'
import { BUTTON_TITLE, USER_NOTICE } from '../strings'
import * as ActionTypes from '../actions'

const FeedSource = ({ url, onSourceSubmit }) => {
  const [feedSource, setFeedSource] = useState('')
  const [isSubmitDisabled, disableSubmit] = useState(false)

  useEffect(() => {
    disableSubmit(_.isEqual(url, feedSource))
  }, [url, feedSource])

  return (
    <Container>
      <SourceInput
        autoFocus
        onChangeText={text => {
          setFeedSource(text)
        }}
        value={feedSource}
        placeholder={USER_NOTICE.SETUP_FEED}
      />

      <Button
        disabled={isSubmitDisabled}
        title={BUTTON_TITLE.SUBMIT}
        onPress={() => onSourceSubmit(feedSource)}
      />
    </Container>
  )
}

const mapStateToProps = ({ url }) => ({ url })
const mapDispatchToProps = dispatch => ({
  onSourceSubmit: feedSource => {
    dispatch({
      type: ActionTypes.ON_SOURCE_SUBMIT,
      payload: { feedSource },
    })
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedSource)

const Container = styled.View`
  display: flex;
  flex: 1;
  flex-basis: 40%;
  align-self: center;
  align-items: center;
  justify-content: center;
`
const SourceInput = styled(TextInput)`
  margin: 10px 20px;
`
