import React, { useEffect } from 'react'
import { Button, ScrollView } from 'react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import ArticleList from '../components/ArticleList'
import { BUTTON_TITLE } from '../strings'
import { SCREEN } from '../routes'
import * as ActionTypes from '../actions'

const HomeScreen = ({
  navigation,
  url,
  articleArr,
  articleCount,
  fetchArticles,
}) => {
  useEffect(() => {
    if (url) fetchArticles(url)
  }, [fetchArticles, url])

  return (
    <ScrollView>
      <Container>
        <Button
          title={BUTTON_TITLE.SETUP_SOURCE}
          onPress={() => {
            navigation.navigate(SCREEN.FEED_SOURCE)
          }}
        />

        <ArticleList
          articleCount={articleCount}
          articleArr={articleArr}
          navigation={navigation}
        />
      </Container>
    </ScrollView>
  )
}

const mapStateToProps = ({ url, articleArr, articleCount }) => ({
  url,
  articleArr,
  articleCount,
})

const mapDispatchToProps = dispatch => ({
  fetchArticles: url =>
    dispatch({
      type: ActionTypes.FETCH_FEED_SOURCE_REQUESTED,
      payload: { url },
    }),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)

const Container = styled.View`
  display: flex;
  align-items: center;
  margin: 10px 0;
`
