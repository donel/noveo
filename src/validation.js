import moment from 'moment'
import * as Yup from 'yup'
import { USER_NOTICE } from './strings'

const urlExp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi
const regexp = new RegExp(urlExp)

export const feedSourceSchema = Yup.string()
  .max(200, USER_NOTICE.URL_TOO_LONG)
  .url(USER_NOTICE.ENTER_VALID_URL)
  .required(USER_NOTICE.ENTER_SOURCE_FEED)

export const validateDate = date => (date ? moment(date).isValid() : false)
export const validateUrl = url => (url ? url.match(regexp) : false)
