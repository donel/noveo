export const BUTTON_TITLE = {
  SETUP_SOURCE: 'Setup the source',
  SUBMIT: 'Submit',
  GO_HOME: 'Go to Home',
  UPDATE_SOURCE: 'Retry Update',
  GO_ARTICLE: 'Go to Article',
}

export const USER_NOTICE = {
  SETUP_FEED: 'Setup feed source',
  SOURCE_NOT_DEFINED: 'The source is not defined',
  REQUEST_FAILED: 'Request Failed',
  URL_TOO_LONG: 'The url is too long!',
  ENTER_VALID_URL: 'Enter a valid url!',
  ENTER_SOURCE_FEED: 'Enter the news source feed!',
}

export const ERR_LOCATION = {
  FETCH_SOURCE_FEED: '[api.fetchSourceFeed]',
}
