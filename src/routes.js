export const SCREEN = {
  HOME: 'Home',
  DETAILS: 'Details',
  FEED_SOURCE: 'FeedSource',
}

export const MODAL = {
  FEED_SOURCE_FAIL: 'FeedSourceFail',
}
