import React from 'react'
import styled from 'styled-components'
import _ from 'lodash'
import { BUTTON_TITLE } from '../strings.js'
import { SCREEN } from '../routes'

const FeedSourceFail = ({ navigation }) => {
  const errorMessage = _.get(navigation.state.params, 'message')

  return (
    <Container>
      <Message>{errorMessage}</Message>

      <BtnContainer>
        <Button
          title={BUTTON_TITLE.GO_HOME}
          onPress={() => navigation.navigate(SCREEN.HOME)}
        />

        <Button
          title={BUTTON_TITLE.UPDATE_SOURCE}
          onPress={() => navigation.goBack()}
        />
      </BtnContainer>
    </Container>
  )
}

export default FeedSourceFail

const Container = styled.View`
  display: flex;
  justify-content: center;
  margin: auto;
`
const Message = styled.Text`
  text-align: center;
  font-weight: 500;
  font-size: 18px;
`
const BtnContainer = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 10px;
  justify-content: space-between;
`
const Button = styled.Button``
