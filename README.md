To install:

```
  yarn
  cd ios/ && pod install
  cd ../
  react-native run-ios || react-native run-android
```

Demo: https://gfycat.com/plushgloomyguillemot
